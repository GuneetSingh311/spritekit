//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by MacStudent on 2019-02-06.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {

    // Example 1  - Adding text to the screen
    let label = SKLabelNode(text:"HELLO WORLD!")
    let label2 = SKLabelNode(text:"ABCD")
    
    // Example 2 - Draw a square on the screen
    let square = SKSpriteNode(color: SKColor.blue, size: CGSize(width: 50, height: 50))
    
    // Example 3 - Draw an image on the screen
    let duck = SKSpriteNode(imageNamed: "psyduck")
    
    // Example 4 - Draw a circle on the screen
    let circle = SKShapeNode(circleOfRadius: 40)
    
    override func didMove(to view: SKView) {
        // output the size of the screen
        print("Screen size (w,h): \(size.width),\(size.height)")
        
        // Add images to the scene
        let bug = SKSpriteNode(imageNamed: "caterpie")
        bug.position = CGPoint(x:size.width/2, y:size.height/2)
        addChild(bug)
        
        duck.position = CGPoint(x:size.width/2+100, y:size.height/2)
        addChild(duck)
        
        
        // configure your text
        label.position = CGPoint(x:size.width/2, y:size.height/2)
        label.fontSize = 45
        label.fontColor = SKColor.yellow

        label2.fontSize = 60
        label2.position = CGPoint(x:size.width/2, y:200)
        
        // add it to your scene (draw it!)
        addChild(label)
        addChild(label2)
        
        // configure the square
        square.position = CGPoint(x: 105, y:700);
        // add square to scene
        addChild(square)
        
        // configure your circle
        // -----------------------
        // color of border
        circle.strokeColor = SKColor.yellow
        // width of border
        circle.lineWidth = 5
        // fill color in the circle
        circle.fillColor = SKColor.magenta
        // location of circle
        circle.position = CGPoint(x:200, y:100)
        addChild(circle)
        
        
        
        let circle2 = SKShapeNode(circleOfRadius: 40)
        circle2.strokeColor = SKColor.yellow
        // width of border
        circle2.lineWidth = 5
        // fill color in the circle
        circle2.fillColor = SKColor.green
        // location of circle
        circle2.position = CGPoint(x:200, y:400)
        addChild(circle2)
        
        
        // Movement---->  SK-action
        //1. single movements
        // 2. Sequence of movements
        
        //1. define a movement
        
        //2. apply movement to the character
        
        let moveAction = SKAction.moveBy(x: 100, y: 550, duration: 2)   // duration is speed.
        // apply movement to the character
        circle.run(moveAction)
        let moveAction2 = SKAction.moveTo(x: 300, duration: 2)  // move to
        square.run(moveAction2)

        // apply move
//        let newPostiton = CGPoint(x: 255, y: 700)
//        let moveAction3 = SKAction.move(to:newPostiton,duration: 4)
//        bug.run(moveAction3)

//        let sequence:SKAction = SKAction.sequence([moveAction2,moveAction])
//        circle.run(sequence.reversed())
//        bug.run(moveAction.reversed())
//
//        let sequence2:SKAction = SKAction.sequence([moveAction,moveAction2])
//        square.run(SKAction.repeatForever(sequence2))
//        circle2.run(SKAction.repeat(sequence2, count: 5))
        if(circle.frame.intersects(square.frame))
        {
        print("Collision detection")
        }
        }
    
    
    override func update(_ currentTime: TimeInterval) {
       // print("time\(currentTime)")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
        let location = touch.location(in:self)
        let middleofScreen = self.size.width/2
        if(location.x<middleofScreen)
        {
            print("tocuhed the left side")
            let moveAction4 = SKAction.moveTo(x: -50, duration: 2)
            square.run(moveAction4)
        }
        else {
            print("touched right")
            let moveAction4 = SKAction.moveTo(x: 500, duration: 2)
            square.run(moveAction4)
        }
        }
        
        
        
        
        
    }
    
}
